#!/usr/bin/env python3
from xml.dom import minidom
import json
import sys
import os

source = sys.argv[1]
destination = sys.argv[2]

def convert_notepad_xml(path,name):
    with open("namelist.json") as f:
        namelist = json.load(f)
    jsondata = {}
    jsondata["name"] = name
    jsondata["id"] = "notepad." + name
    jsondata["global"] = {}
    jsondata["lexer"] = {}
    xmldata = minidom.parse(path)
    for style in xmldata.getElementsByTagName("WidgetStyle"):
        name = style.attributes.get("name").value
        if name == "Global override":
            jsondata["global"]["textColor"] = "#" + style.attributes.get("fgColor").value
            jsondata["global"]["backgroundColor"] = "#" + style.attributes.get("bgColor").value
        elif name == "Current line background colour":
            jsondata["global"]["caretColor"] = "#" + style.attributes.get("bgColor").value
        elif name == "Line number margin":
            jsondata["global"]["marginsForegroundColor"] = "#" + style.attributes.get("fgColor").value
            jsondata["global"]["marginsBackgroundColor"] = "#" + style.attributes.get("bgColor").value
    #Loop through single lexer
    for lexer in xmldata.getElementsByTagName("LexerType"):
        if not lexer.attributes.get("name"):
            continue
        lexername = lexer.attributes.get("name").value
        if not lexername in namelist:
            continue
        current_colors = {}
        for i in lexer.getElementsByTagName("WordsStyle"):
            stylename = i.attributes.get("name").value
            current_colors[stylename] = "#" + i.attributes.get("fgColor").value
        for key,value in namelist[lexername].items():
            if not key in jsondata["lexer"]:
                jsondata["lexer"][key] = {}
            for a,b in value.items():
                if a in current_colors:
                    if isinstance(b,list):
                        for i in b:
                            jsondata["lexer"][key][i] = current_colors[a]
                    else:
                        jsondata["lexer"][key][b] = current_colors[a]
    return jsondata

if os.path.isfile(source):
    st = convert_notepad_xml(source,source[:-4])
    with open(destination,"w") as f:
        json.dump(st, f, ensure_ascii=False, indent=4)
elif os.path.isdir(source):
    for i in os.listdir(source):
        st = convert_notepad_xml(os.path.join(sys.argv[1],i),i[:-4])
        with open(os.path.join(destination,i[:-4] + ".json"),"w") as f:
            json.dump(st, f, ensure_ascii=False, indent=4)
else:
    print("Source was not found",file=sys.stderr)
